# brain-games

[![Actions Status](https://github.com/ilya-rodin/frontend-project-44/workflows/hexlet-check/badge.svg)](https://github.com/ilya-rodin/frontend-project-44/actions)
[![Maintainability](https://api.codeclimate.com/v1/badges/5b8472fed951369b2a15/maintainability)](https://codeclimate.com/github/ilya-rodin/frontend-project-44/maintainability)

## Introduction

Brain Games is a console application that is written in pure JavaScript. With this application, you can play different math games right in the console. You can play 5 different math games.

Available games: _even, calc, gcd, progression, prime_


## Minimum requirements

Node.js version: 18.14.2 LTS

npm version: 9.5.0

## Install

```bash
git clone https://github.com/ilya-rodin/brain-games
cd frontend-project-44/
make install
npm link
```

[![asciicast](https://asciinema.org/a/574197.svg)](https://asciinema.org/a/574197)

## Run

```bash
brain-<game name>
```

## Games demo

<table style="width: 100%;">
    <tr>
        <th style="width: 100%;font-size: 25px;font-weight: bold;text-align: center;" colspan="2">brain-even</th>
        <td style="width: 50%;">
            <a href="https://asciinema.org/a/574180"><img src="https://asciinema.org/a/574180.svg" /></a>
        </td>
    </tr>
    <tr>
        <th style="width: 100%;font-size: 25px;font-weight: bold;text-align: center;" colspan="2">brain-calc</th>
        <td style="width: 50%;">
            <a href="https://asciinema.org/a/574183"><img src="https://asciinema.org/a/574183.svg" /></a>
        </td>
    </tr>
    <tr>
        <th style="width: 100%;font-size: 25px;font-weight: bold;text-align: center;" colspan="2">brain-gcd</th>
        <td>
            <a href="https://asciinema.org/a/574187"><img src="https://asciinema.org/a/574187.svg" /></a>
        </td>
    </tr>
    <tr>
        <th style="width: 100%;font-size: 25px;font-weight: bold;text-align: center;" colspan="2">brain-progression</th>
        <td>
            <a href="https://asciinema.org/a/574189"><img src="https://asciinema.org/a/574189.svg" /></a>
        </td>
    </tr>
    <tr>
        <th style="width: 100%;font-size: 25px;font-weight: bold;text-align: center;" colspan="2">brain-prime</th>
        <td>
            <a href="https://asciinema.org/a/574190"><img src="https://asciinema.org/a/574190.svg" /></a>
        </td>
    </tr>
</table>
